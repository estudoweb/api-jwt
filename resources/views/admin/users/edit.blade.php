@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <h3>Editar Usuário</h3>
        {!! form($form->add('salve','submit', [
         'attr' => ['class' => 'btn btn-primary btn-block'],
            'label' => 'Salvar'
         ])) !!}
    </div>
</div>
@endsection
