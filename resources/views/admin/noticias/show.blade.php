@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <h3>Visualizar Notícia</h3>

        <div class="row">
            {!! Button::primary('Editar')->asLinkTo(route('admin.noticias.edit', ['noticia' => $noticia->id])) !!}
            {!! Button::danger('Excluir')->asLinkTo(route('admin.noticias.destroy', ['noticia' => $noticia->id]))
                ->addAttributes(['onclick' => "event.preventDefault(); document.getElementById(\"form-delete\").submit();"])
            !!}
            <?php $formDelete = FormBuilder::plain([
                                                    'id' => 'form-delete',
                                                    'route' => ['admin.noticias.destroy', 'noticia' => $noticia->id],
                                                    'method' => 'DELETE',
                                                    'style' => 'display:none'
                                                    ]
                                                );
                    ?>
            {!! form($formDelete) !!}

            <br />
            <br />
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td scope="row">#</td>
                    <td>{{$noticia->id}}</td>
                </tr>
                <tr>
                    <td scope="row">Título</td>
                    <td>{{$noticia->titulo}}</td>
                </tr>
                <tr>
                    <td scope="row">Descrição</td>
                    <td>{{$noticia->descricao}}</td>
                </tr>

                </tbody>
            </table>
        </div>
        
    </div>
</div>
@endsection
