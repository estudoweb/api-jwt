@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <h3>Lista de Notícias</h3>
        {!! Button::primary('Nova notícia')->asLinkTo(route('admin.noticias.create')) !!}
    </div>
    <div class="row">
       {!! Table::withContents($noticias->items())->striped()
            ->callback('Ações', function($field, $noticia) {
                $linkEdit = route('admin.noticias.edit', ['noticia' => $noticia->id]);
                $linkShow = route('admin.noticias.show', ['noticia' => $noticia->id]);
                return Button::link(Icon::create('pencil'))->asLinkTo($linkEdit) . "|" .
                        Button::link(Icon::create('remove'))->asLinkTo($linkShow);
            })
        !!}

        {!! $noticias->links() !!}
    </div>
</div>
@endsection
