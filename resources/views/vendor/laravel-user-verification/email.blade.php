<h3>{{config('app.name')}}</h3>

<p>Sua conta na plataforma foi criada.</p>

<p>
    <a href="{{ $link = route('email-verification.check', $user->verification_token) . '?email=' . urlencode($user->email) }}">Clique aqui para ativar.</a>
</p>