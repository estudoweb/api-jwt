<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class NoticiaForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('titulo', 'text', [
                'rules' => 'required'
            ]);

        $this
            ->add('descricao', 'textarea', [
                'rules' => 'required'
            ]);
    }
}
