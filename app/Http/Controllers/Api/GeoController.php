<?php

namespace App\Http\Controllers\Api;

use App\Models\Geo;
use App\Repositories\GeoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;


class GeoController extends Controller
{

    private $repository;

    /**
     * UsersController constructor.
     */
    public function __construct(GeoRepository $repository)
    {
        $this->repository = $repository;
    }



    public function store(Request $request)
    {
        $data = [
            'geo' => $request->request->get('geo'),
            'id_user' => JWTAuth::parseToken()->toUser()->id
        ];

        $this->repository->create($data);


        return response()->json(['Cadastrado com sucesso'], 200);
    }
}
