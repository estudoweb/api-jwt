<?php

namespace App\Http\Controllers\Admin;

use App\Forms\NoticiaForm;
use App\Models\Noticia;
use App\Repositories\NoticiaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class NoticiaController extends Controller
{

    private $repository;

    /**
     * UsersController constructor.
     */
    public function __construct(NoticiaRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $noticias = $this->repository->paginate();
        return view('admin.noticias.index', compact('noticias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form = \FormBuilder::create(NoticiaForm::class, [
            'url' => route('admin.noticias.store'),
            'method' => 'POST'
        ]);

        return view('admin.noticias.create', compact('form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $form = \FormBuilder::create(NoticiaForm::class);

        if(!$form->isValid()){
            return redirect()
                ->back()
                ->withErrors($form->getErrors())
                ->withInput();
        }

        $data = $form->getFieldValues();
        $this->repository->create($data);

        $request->session()->flash('message','Notícia criada com sucesso.');

        return redirect()->route('admin.noticias.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Noticia $noticia)
    {
        return view('admin.noticias.show', compact('noticia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Noticia $noticia)
    {
        $form = \FormBuilder::create(NoticiaForm::class, [
            'url' => route('admin.noticias.update', ['user' => $noticia->id]),
            'method' => 'PUT',
            'model' => $noticia
        ]);

        return view('admin.noticias.edit', compact('form'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $form = \FormBuilder::create(NoticiaForm::class);

        if(!$form->isValid()){
            return redirect()
                ->back()
                ->withErrors($form->getErrors())
                ->withInput();
        }

        $data = array_except($form->getFieldValues(), ['password','role']);
        $this->repository->update($data, $id);

        $request->session()->flash('message','Notícia alterada com sucesso.');
        return redirect()->route('admin.noticias.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        return redirect()->route('admin.noticias.index');
    }
}
